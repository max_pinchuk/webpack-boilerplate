# Webpack template for static websites

## Overview
Simple webpack template for static websites

## Usage

**1. Clone repository**

**2. Install dependencies**
```
npm i
```

**3. Use it**

Development mode
```
npm run dev
```

Production mode
```
npm run build
```